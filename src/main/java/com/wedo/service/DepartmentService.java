package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DepartmentDTO;

public interface DepartmentService {

	ResponseModelList<DepartmentDTO> save(DepartmentDTO departmentDTO);
	ResponseModelList<DepartmentDTO> findById(String departmentId);
	ResponseModelList<DepartmentDTO> findAll();
	ResponseModelList<DepartmentDTO> findAllActive();
	ResponseModelList<DepartmentDTO> deleteAll();
	ResponseModelList<DepartmentDTO> deleteById(String departmentdId);
}
