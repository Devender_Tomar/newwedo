package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DesignationDTO;

public interface DesignationService {
	
	ResponseModelList<DesignationDTO> save(DesignationDTO designationDTO);
	ResponseModelList<DesignationDTO> findById(String departmentId);
	ResponseModelList<DesignationDTO> findAll();
	ResponseModelList<DesignationDTO> findAllActive();
	ResponseModelList<DesignationDTO> deleteAll();
	ResponseModelList<DesignationDTO> deleteById(String departmentId);
	
}
