package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.ClientDTO;

public interface ClientService {

	ResponseModelList<ClientDTO> save(ClientDTO clientDTO);
	ResponseModelList<ClientDTO> findById(String clientId);
	ResponseModelList<ClientDTO> deleteById(String clientId);
	ResponseModelList<ClientDTO> findAll();
	ResponseModelList<ClientDTO> findAllActive();
	ResponseModelList<ClientDTO> deleteAll();
	/* ResponseModelList<ClientDTO> deleteByIds(List<String> clientId); */
	
}
