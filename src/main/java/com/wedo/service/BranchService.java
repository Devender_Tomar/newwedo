package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.BranchDTO;

public interface BranchService {

	ResponseModelList<BranchDTO> save(BranchDTO branchDTO);
	ResponseModelList<BranchDTO> findById(String branchId);
	ResponseModelList<BranchDTO> findAll();
	ResponseModelList<BranchDTO> findAllActive();
	ResponseModelList<BranchDTO> deleteAll();
	ResponseModelList<BranchDTO> deleteById(String branchId);
	
}
