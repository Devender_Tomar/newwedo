package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.EmployeeDTO;

public interface EmployeeService {

	ResponseModelList<EmployeeDTO> save(EmployeeDTO employeeDTO);
	ResponseModelList<EmployeeDTO> findById(String employeeId);
	ResponseModelList<EmployeeDTO> findAll();
	ResponseModelList<EmployeeDTO> findAllActive();
	ResponseModelList<EmployeeDTO> findAllDeleted();
	ResponseModelList<EmployeeDTO> deleteById(String employeeId);
	ResponseModelList<EmployeeDTO> deleteAll();
}
