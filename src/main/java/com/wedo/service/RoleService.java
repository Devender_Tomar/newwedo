package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.RoleDTO;

public interface RoleService {

	ResponseModelList<RoleDTO> save(RoleDTO roleDTO);
	ResponseModelList<RoleDTO> findById(String roleId);
	ResponseModelList<RoleDTO> deleteById(String roleId);
	ResponseModelList<RoleDTO> findAll();
	ResponseModelList<RoleDTO> findAllActive();
	ResponseModelList<RoleDTO> deleteAll();

}
