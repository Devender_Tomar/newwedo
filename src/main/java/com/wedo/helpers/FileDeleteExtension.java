package com.wedo.helpers;

import java.io.File;

import org.apache.tomcat.util.http.fileupload.FileUtils;

public class FileDeleteExtension implements Runnable {

	File file;
	long threadTime;

	public FileDeleteExtension(File file, long threadTime) {
		this.file = file;
		this.threadTime = threadTime;
	}

	@Override
	public void run() {
		try {
			HelperExtension.Print("Thread Sleep");
			Thread.sleep(threadTime);
			HelperExtension.Print("Thread Starts");
			if (file.exists()) {
				HelperExtension.Print("File Path ->" + file.getAbsolutePath());
				FileUtils.forceDelete(file);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
