package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.AddressDAO;
import com.wedo.dao.BranchDAO;
import com.wedo.dao.OfficeAddressDAO;
import com.wedo.dto.BranchDTO;
import com.wedo.dto.ClientDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Address;
import com.wedo.model.Branch;
import com.wedo.model.Client;
import com.wedo.model.OfficeAddress;
import com.wedo.service.BranchService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BranchServiceImpl implements BranchService {

	@Autowired
	AddressDAO addressDAO;

	@Autowired
	BranchDAO branchDAO;

	ResponseModelList<BranchDTO> response = new ResponseModelList<BranchDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<BranchDTO> list = null;

	@Override
	public ResponseModelList<BranchDTO> save(BranchDTO branchDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving branch information");
			if (!helperExtension.isNullOrEmpty(branchDTO.getBranchId())) {
				putValueInResponseModel(true, ConstantExtension.BRANCH_UPDATED, branchDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.BRANCH_ADDED, branchDTO);
			}
			log.info(message);
			response = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;
	}

	@Override
	public ResponseModelList<BranchDTO> findById(String branchId) {
		try {
			list = new ArrayList<BranchDTO>();
			if (!branchId.isEmpty()) {
				Branch branch = branchDAO.findById(branchId).get();
				if (branch != null) {
					BranchDTO dto = mapper.map(branch, BranchDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.BRANCH_FETCHED, list);
				}
			}
			log.info(ConstantExtension.BRANCH_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.BRANCH_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<BranchDTO> findAll() {
		try {
			list = new ArrayList<BranchDTO>();
			List<Branch> branchs= branchDAO.findAll();
			for( Branch branch: branchs)
			{
				BranchDTO branchDTO=mapper.map(branch, BranchDTO.class);
				list.add(branchDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.BRANCHS_FETCHED, list);
			log.info(ConstantExtension.BRANCH_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.BRANCH_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;
	}

	
	@Override
	public ResponseModelList<BranchDTO> findAllActive() {
		try {
			list = new ArrayList<BranchDTO>();
			List<Branch> branchs= branchDAO.findByIsFlag(1);
			for( Branch branch: branchs)
			{
				BranchDTO branchDTO=mapper.map(branch, BranchDTO.class);
				list.add(branchDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.BRANCHS_FETCHED, list);
			log.info(ConstantExtension.BRANCH_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.BRANCH_NOT_EXISTS, list);
			e.printStackTrace();
		}
		return response;

	}

	@Override
	public ResponseModelList<BranchDTO> deleteAll() {
		try {
			list = new ArrayList<BranchDTO>();
			List<Branch> branchs= branchDAO.findByIsFlag(1);
			for( Branch branch: branchs)
			{
				branch.setIsFlag(0);
				branch=branchDAO.save(branch);
				BranchDTO branchDTO=mapper.map(branch, BranchDTO.class);
				list.add(branchDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.BRANCH_DELETED, list);
			log.info(ConstantExtension.BRANCH_DELETED);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.BRANCH_NOT_EXISTS, list);
			e.printStackTrace();
		}
		return response;

	}

	@Override
	public ResponseModelList<BranchDTO> deleteById(String branchId) {
		try {
			list = new ArrayList<BranchDTO>();
			if (!branchId.isEmpty()) {
				Branch branch = branchDAO.findById(branchId).get();
				if (branch != null) {
					branch.setIsFlag(0);
					branch=branchDAO.save(branch);
					BranchDTO dto = mapper.map(branch, BranchDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.BRANCH_DELETED, list);
				}
			}
			log.info(ConstantExtension.BRANCH_DELETED);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.BRANCH_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;
	}

	private void putValueInResponseModel(boolean status, String message, BranchDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Branch model = mapper.map(dto, Branch.class);
			if (!helperExtension.isNullOrEmpty(dto.getBranchId())) {
				model = mapper.map(dto, Branch.class);
				model.setBranchId(dto.getBranchId());
				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Branch.class);
				model.setBranchId("br" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = branchDAO.save(model);
			dto = mapper.map(model, BranchDTO.class);
			list.add(dto);
		}
	}

}
