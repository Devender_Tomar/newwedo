package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.DesignationDAO;
import com.wedo.dto.BranchDTO;
import com.wedo.dto.DepartmentDTO;
import com.wedo.dto.DesignationDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Branch;
import com.wedo.model.Designation;
import com.wedo.service.DepartmentService;
import com.wedo.service.DesignationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DesignationServiceImpl implements DesignationService {

	@Autowired
	DesignationDAO dao;

	ResponseModelList<DesignationDTO> response = new ResponseModelList<DesignationDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<DesignationDTO> list = null;

	@Override
	public ResponseModelList<DesignationDTO> save(DesignationDTO departmentDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Designation information");
			if (!helperExtension.isNullOrEmpty(departmentDTO.getDesignationId())) {
				putValueInResponseModel(true, ConstantExtension.DESIGNATION_UPDATED, departmentDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.DESIGNATION_ADDED, departmentDTO);
			}
			log.info(message);
			response = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;
	}

	@Override
	public ResponseModelList<DesignationDTO> findById(String departmentId) {
		try {
			list = new ArrayList<DesignationDTO>();
			if (!departmentId.isEmpty()) {
				Designation designation = dao.findById(departmentId).get();
				if (designation != null) {
					DesignationDTO dto = mapper.map(designation, DesignationDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
				}
			}
			log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<DesignationDTO> findAll() {
		try {
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findAll();
			for (Designation designation : designations)
				if (designation != null) {
					DesignationDTO dto = mapper.map(designation, DesignationDTO.class);
					list.add(dto);

				}
			response = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<DesignationDTO> findAllActive() {
		try {
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findByIsFlag(1);
			for (Designation designation : designations)
				if (designation != null) {
					DesignationDTO dto = mapper.map(designation, DesignationDTO.class);
					list.add(dto);

				}
			response = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<DesignationDTO> deleteAll() {
		try {
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findByIsFlag(1);
			for (Designation designation : designations)
				if (designation != null) {
					designation.setIsFlag(0);
					designation=dao.save(designation);
					DesignationDTO dto = mapper.map(designation, DesignationDTO.class);
					list.add(dto);

				}
			response = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public ResponseModelList<DesignationDTO> deleteById(String departmentId) {
		try {
			list = new ArrayList<DesignationDTO>();
			if (!departmentId.isEmpty()) {
				Designation designation = dao.findById(departmentId).get();
				if (designation != null) {
					designation.setIsFlag(0);
					designation=dao.save(designation);
					DesignationDTO dto = mapper.map(designation, DesignationDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
				}
			}
			log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
		} catch (Exception e) {
			log.error("Error while finding branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}

		return response;
	}

	private void putValueInResponseModel(boolean status, String message, DesignationDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Designation model = mapper.map(dto, Designation.class);
			if (!helperExtension.isNullOrEmpty(dto.getDesignationId())) {
				model.setDesignationId(dto.getDesignationId());
				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Designation.class);
				model.setDesignationId("des" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = dao.save(model);
			dto = mapper.map(model, DesignationDTO.class);
			list.add(dto);
		}
	}

}
