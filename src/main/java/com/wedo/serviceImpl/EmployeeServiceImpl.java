package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.AddressDAO;
import com.wedo.dao.EmployeeDAO;
import com.wedo.dto.EmployeeDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Address;
import com.wedo.model.Employee;
import com.wedo.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;

	@Autowired
	AddressDAO addressDAO;

	ResponseModelList<EmployeeDTO> responseModel = new ResponseModelList<EmployeeDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<EmployeeDTO> list = null;

	@Override
	public ResponseModelList<EmployeeDTO> save(EmployeeDTO employeeDTO) {
		try {
			list = new ArrayList<EmployeeDTO>();
			log.info("Saving employee information");
			if (!helperExtension.isNullOrEmpty(employeeDTO.getEmployeeId())) {
				putValueInResponseModel(true, ConstantExtension.EMPLOYEE_UPDATED, employeeDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.EMPLOYEE_ADDED, employeeDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("Error while saving employee information");
			responseModel = new ResponseModelList<>(false, "Employee Not saved", list);
			e.printStackTrace();

		}
		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findById(String employeeId) {

		try {
			list = new ArrayList<>();
			Employee employee = employeeDAO.findById(employeeId).get();
			if (employee != null) {
				EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
				list.add(dto);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while find employee by Id");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findAll() {
		try {
			list = new ArrayList<>();
			List<Employee> employeelist = employeeDAO.findAll();
			for(Employee employee: employeelist)
			{
				if (employee != null) {
					EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
					list.add(dto);
				}	
			}
			log.error("Sucessfully found all employee");
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding all employee");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findAllActive() {
		try {
			list = new ArrayList<>();
			List<Employee> employeelist = employeeDAO.findByIsFlag(1);
			for(Employee employee: employeelist)
			{
				if (employee != null) {
					EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
					list.add(dto);
				}	
			}
			log.error("Sucessfully found Employees");
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while find all employee");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<EmployeeDTO> findAllDeleted() {
		try {
			list = new ArrayList<>();
			List<Employee> employeelist = employeeDAO.findByIsFlag(0);
			for(Employee employee: employeelist)
			{
				if (employee != null) {
					EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
					list.add(dto);
				}	
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding deleted employee");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<EmployeeDTO> deleteById(String employeeId) {
		try {
			list = new ArrayList<>();
			Employee employee = employeeDAO.findById(employeeId).get();
			if (employee != null) {
				employee.setIsFlag(0);
				employee=employeeDAO.save(employee);
				EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
				list.add(dto);
			}
			log.info("Employee Successfully Deleted");
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_DELETED, list);
		} catch (Exception e) {
			log.error("Error while deleting employee by Id");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<EmployeeDTO> deleteAll() {
		try {
			list = new ArrayList<>();
			List<Employee> employeelist = employeeDAO.findByIsFlag(1);
			for(Employee employee: employeelist)
			{
				if (employee != null) {
					EmployeeDTO dto = mapper.map(employee, EmployeeDTO.class);
					list.add(dto);
				}	
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding deleted employee");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
			e.printStackTrace();
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, EmployeeDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		Address address = new Address();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Employee model = mapper.map(dto, Employee.class);
			if (!helperExtension.isNullOrEmpty(dto.getEmployeeId())) {
				model = mapper.map(dto, Employee.class);
				model.setEmployeeId(dto.getEmployeeId());

				if (!model.getAddress().getAddressid().isEmpty())
					model.setAddress(sendAddress(model.getAddress()));

				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Employee.class);
				model.setEmployeeId("emp" + helperExtension.getUniqueId());
				model.getAddress().setAddressid("add" + helperExtension.getUniqueId());
				model.setAddress(sendAddress(model.getAddress()));
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = employeeDAO.save(model);
			dto = mapper.map(model, EmployeeDTO.class);
			list.add(dto);
		}
	}

	public Address sendAddress(Address address) {
		try {
			address = addressDAO.save(address);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return address;
	}

}
