package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.RoleDAO;
import com.wedo.dto.RoleDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Role;
import com.wedo.service.RoleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

	ResponseModelList<RoleDTO> response = new ResponseModelList<RoleDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<RoleDTO> list = null;

	@Autowired
	RoleDAO roleDao;

	@Override
	public ResponseModelList<RoleDTO> save(RoleDTO roleDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving role information");
			if(roleDao.findByRole(roleDTO.getRole()).size()>0)
			{
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_ALREADY_EXISTS, null);
			return response;
			}
			
			if (!helperExtension.isNullOrEmpty(roleDTO.getRoleId())) {
				putValueInResponseModel(true, ConstantExtension.ROLE_UPDATED, roleDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.ROLE_ADDED, roleDTO);
			}
			log.info(message);
			response = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving role information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;
	}

	@Override
	public ResponseModelList<RoleDTO> findById(String roleId) {
		try {
			list = new ArrayList<RoleDTO>();
			if (!roleId.isEmpty()) {
				Role branch = roleDao.findById(roleId).get();
				if (branch != null) {
					RoleDTO dto = mapper.map(branch, RoleDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.ROLES_FETCHED, list);
				}
			}
			log.info(ConstantExtension.ROLES_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding role by id");
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<RoleDTO> deleteById(String roleId) {
		try {
			list = new ArrayList<RoleDTO>();
			if (!roleId.isEmpty()) {
				Role role = roleDao.findById(roleId).get();
				if (role != null) {
					role.setIsFlag(0);
					role = roleDao.save(role);
					RoleDTO dto = mapper.map(role, RoleDTO.class);
					list.add(dto);
					response = new ResponseModelList<>(true, ConstantExtension.ROLE_DELETED, list);
				}
			}
			log.info(ConstantExtension.ROLE_DELETED);
		} catch (Exception e) {
			log.error("Error while deleting branch by id");
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public ResponseModelList<RoleDTO> findAll() {
		try {
			list = new ArrayList<RoleDTO>();
			List<Role> roles = roleDao.findAll();
			if(roles.size()>0)
			for (Role role : roles) {
				RoleDTO roleDTO = mapper.map(role, RoleDTO.class);
				list.add(roleDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.ROLE_FETCHED, list);
			log.info(ConstantExtension.ROLE_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding all role");
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_NOT_EXISTS, list);
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseModelList<RoleDTO> findAllActive() {
		try {
			list = new ArrayList<RoleDTO>();
			List<Role> roles = roleDao.findByIsFlag(1);
			for (Role role : roles) {
				RoleDTO roleDTO = mapper.map(role, RoleDTO.class);
				list.add(roleDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.ROLE_FETCHED, list);
			log.info(ConstantExtension.ROLE_FETCHED);
		} catch (Exception e) {
			log.error("Error while finding all active");
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_NOT_EXISTS, list);
			e.printStackTrace();
		}
		return response;

	}

	@Override
	public ResponseModelList<RoleDTO> deleteAll() {
		try {
			list = new ArrayList<RoleDTO>();
			List<Role> roles = roleDao.findByIsFlag(1);
			for (Role role : roles) {
				role.setIsFlag(0);
				role = roleDao.save(role);
				RoleDTO roleDTO = mapper.map(role, RoleDTO.class);
				list.add(roleDTO);
			}
			response = new ResponseModelList<>(true, ConstantExtension.ROLE_DELETED, list);
			log.info(ConstantExtension.ROLE_DELETED);
		} catch (Exception e) {
			log.error("Error while delete all");
			response = new ResponseModelList<>(false, ConstantExtension.ROLE_NOT_EXISTS, list);
			e.printStackTrace();
		}
		return response;

	}

	private void putValueInResponseModel(boolean status, String message, RoleDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Role model = mapper.map(dto, Role.class);
			if (!helperExtension.isNullOrEmpty(dto.getRoleId())) {
				model = mapper.map(dto, Role.class);
				model.setRoleId(dto.getRoleId());
				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Role.class);
				model.setRoleId("br" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = roleDao.save(model);
			dto = mapper.map(model, RoleDTO.class);
			list.add(dto);
		}
	}

}
