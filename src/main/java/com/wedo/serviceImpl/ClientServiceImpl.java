package com.wedo.serviceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.AddressDAO;
import com.wedo.dao.ClientDAO;
import com.wedo.dao.OfficeAddressDAO;
import com.wedo.dto.ClientDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Address;
import com.wedo.model.Client;
import com.wedo.model.OfficeAddress;
import com.wedo.service.ClientService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientDAO clientDAO;

	@Autowired
	AddressDAO addressDAO;

	@Autowired
	OfficeAddressDAO officeAddressDAO;

	ResponseModelList<ClientDTO> responseModel = new ResponseModelList<ClientDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<ClientDTO> list = null;

	@Override
	public ResponseModelList<ClientDTO> save(ClientDTO clientDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Client information");
			if (!helperExtension.isNullOrEmpty(clientDTO.getClientId())) {
				putValueInResponseModel(true, ConstantExtension.CLIENT_UPDATED, clientDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.CLIENT_ADDED, clientDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findById(String clientId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseModelList<ClientDTO> deleteById(String clientId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseModelList<ClientDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseModelList<ClientDTO> deleteAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseModelList<ClientDTO> findAllActive() {
		try {
			list = new ArrayList<ClientDTO>();
			log.info("Finding All Active Client");
			List<Client> clients = clientDAO.findByIsFlag(1);
			for (Client client : clients) {
				if(!client.getClientImage().isEmpty())
				{
					File file=new File(client.getClientImage());
					client.setClientImage(file.getName());
				}
					
				ClientDTO clientDTO = mapper.map(client, ClientDTO.class);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching All Active Client Information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, ClientDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		Address address = new Address();
		OfficeAddress officeAddress = new OfficeAddress();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Client model = mapper.map(dto, Client.class);
			if (!helperExtension.isNullOrEmpty(dto.getClientId())) {
				model = mapper.map(dto, Client.class);
				model.setClientId(dto.getClientId());
				
				if (!model.getAddress().getAddressid().isEmpty())
					model.setAddress(sendAddress(model.getAddress()));
				
				if (!model.getOfficeAddress().getOfficeAddressId().isEmpty())
					model.setOfficeAddress(sendOfficeAddress(model.getOfficeAddress()));
				
				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Client.class);
				model.setClientId("cli" + helperExtension.getUniqueId());
				model.getAddress().setAddressid("add" + helperExtension.getUniqueId());
				model.setAddress(sendAddress(model.getAddress()));
				model.getOfficeAddress().setOfficeAddressId("off" + helperExtension.getUniqueId());
				model.setOfficeAddress(sendOfficeAddress(model.getOfficeAddress()));
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = clientDAO.save(model);
			dto = mapper.map(model, ClientDTO.class);
			list.add(dto);
		}
	}

	public Address sendAddress(Address address) {
		try {
			address = addressDAO.save(address);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return address;
	}

	public OfficeAddress sendOfficeAddress(OfficeAddress address) {
		try {
			address = officeAddressDAO.save(address);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return address;
	}

}
