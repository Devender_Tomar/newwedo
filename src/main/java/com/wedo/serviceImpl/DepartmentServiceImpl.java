package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.DepartmentDAO;
import com.wedo.dto.DepartmentDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Department;
import com.wedo.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	DepartmentDAO departmentDAO;

	ResponseModelList<DepartmentDTO> response = new ResponseModelList<DepartmentDTO>();

	HelperExtension helperExtension = new HelperExtension();

	ModelMapper mapper = new ModelMapper();

	private boolean status = false;

	private String message = "";

	private List<DepartmentDTO> list = null;

	@Override
	public ResponseModelList<DepartmentDTO> save(DepartmentDTO departmentDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving department information");
			if (!helperExtension.isNullOrEmpty(departmentDTO.getDepartmentId())) {
				putValueInResponseModel(true, ConstantExtension.DEPARTMENT_UPDATED, departmentDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.DEPARTMENT_ADDED, departmentDTO);
			}
			log.info(message);
			response = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;

	}

	@Override
	public ResponseModelList<DepartmentDTO> findById(String departmentId) {
		try {
			list = new ArrayList<>();

			Department department = departmentDAO.findById(departmentId).get();
			if (department != null) {
				DepartmentDTO departmentDTO = mapper.map(department, DepartmentDTO.class);
				list.add(departmentDTO);
			}
			log.info(ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE);
			response = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("error while findById department information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;
	}

	@Override
	public ResponseModelList<DepartmentDTO> findAll() {
		try {
			list = new ArrayList<>();
			List<Department> departments = departmentDAO.findAll();
			for (Department department : departments)
				if (department != null) {
					DepartmentDTO departmentDTO = mapper.map(department, DepartmentDTO.class);
					list.add(departmentDTO);
				}
			log.info(ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE);
			response = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("error while finding all department information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;
	}

	@Override
	public ResponseModelList<DepartmentDTO> findAllActive() {
		try {
			list = new ArrayList<>();
			List<Department> departments = departmentDAO.findByIsFlag(1);
			for (Department department : departments)
				if (department != null) {
					DepartmentDTO departmentDTO = mapper.map(department, DepartmentDTO.class);
					list.add(departmentDTO);
				}
			log.info(ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE);
			response = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("error while finding all active department information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;	
		}

	@Override
	public ResponseModelList<DepartmentDTO> deleteAll() {
		try {
			list = new ArrayList<>();
			List<Department> departments = departmentDAO.findByIsFlag(1);
			for (Department department : departments)
				if (department != null) {
					department.setIsFlag(0);
					department=departmentDAO.save(department);
					DepartmentDTO departmentDTO = mapper.map(department, DepartmentDTO.class);
					list.add(departmentDTO);
				}
			log.info(ConstantExtension.DEPARTMENT_DELETED);
			response = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_DELETED, list);
		} catch (Exception e) {
			log.error("error while deleting department information");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;	
	}

	@Override
	public ResponseModelList<DepartmentDTO> deleteById(String departmentId) {
		try {
			list = new ArrayList<>();
			Department department = departmentDAO.findById(departmentId).get();
				if (department != null) {
					department.setIsFlag(0);
					department=departmentDAO.save(department);
					DepartmentDTO departmentDTO = mapper.map(department, DepartmentDTO.class);
					list.add(departmentDTO);
				}
			log.info(ConstantExtension.DEPARTMENT_DELETED);
			response = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_DELETED, list);
		} catch (Exception e) {
			log.error("error while deleting department information by id");
			e.printStackTrace();
			response = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return response;	
	}

	private void putValueInResponseModel(boolean status, String message, DepartmentDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {

			Department model = mapper.map(dto, Department.class);
			if (!helperExtension.isNullOrEmpty(dto.getDepartmentId())) {
				model = mapper.map(dto, Department.class);
				model.setDepartmentId(dto.getDepartmentId());
				model.setUpdatedOn(dateTime);
			} else {
				model = mapper.map(dto, Department.class);
				model.setDepartmentId("dep" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = departmentDAO.save(model);
			dto = mapper.map(model, DepartmentDTO.class);
			list.add(dto);
		}
	}

}
