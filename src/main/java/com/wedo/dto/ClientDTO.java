package com.wedo.dto;

import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDTO extends BaseDTO{
	
	/* * private Role role;
	  *  private Branch branch; private Ledger ledger;
	 */
	private String clientId;
	private String clientFirstName;
	private String clientMiddleName;
	private String clientLastName;
	private String clientImage;
	private String clientEmailId;
	private String clientAadharCard;
	private String clientPanCard;
	private String clientGstInNo;
	private String clientGender;
	private String clientContactNo1;
	private String clientContactNo2;
	private String clientWhatsappNo;
	private String clientEmergencyNo;
	private String clientCompanyName;
	private String clientWebsite;
	private String clientType;
	private String fax;
	private String designation;
	private AddressDTO address=new AddressDTO();
	
	private OfficeAddressDTO officeAddress=new OfficeAddressDTO();
	/*
	 * private List<Ledger> ledgers = new ArrayList<Ledger>(0); private
	 * List<Document> documents = new ArrayList<Document>(0); private List<Project>
	 * projects = new ArrayList<Project>(0); private List<Feedback> feedbacks = new
	 * ArrayList<Feedback>(0); private List<Invoice> invoices = new
	 * ArrayList<Invoice>(0); private List<Maintenance> maintenances = new
	 * ArrayList<Maintenance>(0);
	 */
}
