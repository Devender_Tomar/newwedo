package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class EmployeeTypeDTO  extends BaseDTO{

	private String empTypeId;
	private String type;
	private List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>(0);
}
