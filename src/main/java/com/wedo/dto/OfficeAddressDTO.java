package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.wedo.baseModelAndDto.BaseDTO;
import com.wedo.model.Client;
import com.wedo.model.Employee;

import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfficeAddressDTO  extends BaseDTO {
	
	private String officeAddressId;
	private String addressStreet1;
	private String addressStreet2;
	private String city;
	private String pincode;
	private String state;
	private String country;
	
	@JsonIgnore
	private List<ClientDTO> clients = new ArrayList<ClientDTO>(0);

}
