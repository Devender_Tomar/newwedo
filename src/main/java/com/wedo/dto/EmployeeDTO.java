package com.wedo.dto;

import java.util.Date;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class EmployeeDTO  extends BaseDTO{

	private String employeeId;
	private AddressDTO address;
	private BranchDTO branch;
	private DepartmentDTO department;
	private DesignationDTO designation;
	private EmployeeTypeDTO employeeType;
	private RoleDTO role;
	private String firstname;
	private String lastname;
	private String email;
	private Integer contactno1;
	private Integer contactno2;
	private Integer aadharno;
	private String pancardno;
	private String createdBy;
	private String pfAccount;
	private String lastupdatedBy;
	private Date dob;
	private Integer gender;
	private Date joinngDate;
	private Float experience;
	private String username;
	private String password;
	/*
	 * private List<Salary> salaries = new ArrayList<Salary>(0); private
	 * List<Document> documents = new ArrayList<Document>(0); private List<EmpLeave>
	 * empLeaves = new ArrayList<EmpLeave>(0); private List<Attendence> attendences
	 * = new ArrayList<Attendence>(0); private List<MaintenanceTask>
	 * maintenanceTasks = new ArrayList<MaintenanceTask>(0);
	 */
}
