package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class DepartmentDTO  extends BaseDTO{
	
	private String departmentId;
	private String departmenttype;
	private List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>(0);
}
