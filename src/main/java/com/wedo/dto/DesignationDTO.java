package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.baseModelAndDto.BaseDTO;
import com.wedo.model.Employee;

import lombok.Data;

@Data
public class DesignationDTO  extends BaseDTO {

	private String designationId;
	private String designationtype;
	private List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>(0);
}
