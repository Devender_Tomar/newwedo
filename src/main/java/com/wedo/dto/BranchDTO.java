package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class BranchDTO  extends BaseDTO {

	private String branchId;
	private String branchName;
	private List<ClientDTO> clients = new ArrayList<ClientDTO>(0);
	private List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>(0);
}
