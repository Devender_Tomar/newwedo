package com.wedo.model;
// default package
// Generated May 16, 2020 8:05:05 PM by Hibernate Tools 5.2.10.Final

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Interview generated by hbm2java
 */
@Entity
@Table(name = "interview", catalog = "infix_wedo")
public class Interview extends BaseModel implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 7627046710742870896L;
	private String interviewId;
	private JobTable jobTable;
	private String interviewType;
	private Date interviewDate;
	private Date interviewTime;
	private List<InterviewApplicant> interviewApplicants = new ArrayList<InterviewApplicant>(0);

	public Interview() {
	}

	public Interview(String interviewId) {
		this.interviewId = interviewId;
	}

	public Interview(String interviewId, JobTable jobTable, String interviewType, Date interviewDate,
			Date interviewTime, List<InterviewApplicant> interviewApplicants) {
		this.interviewId = interviewId;
		this.jobTable = jobTable;
		this.interviewType = interviewType;
		this.interviewDate = interviewDate;
		this.interviewTime = interviewTime;
		this.interviewApplicants = interviewApplicants;
	}

	@Id

	@Column(name = "interview_id", unique = true, nullable = false, length = 21)
	public String getInterviewId() {
		return this.interviewId;
	}

	public void setInterviewId(String interviewId) {
		this.interviewId = interviewId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	public JobTable getJobTable() {
		return this.jobTable;
	}

	public void setJobTable(JobTable jobTable) {
		this.jobTable = jobTable;
	}

	@Column(name = "interview_type", length = 30)
	public String getInterviewType() {
		return this.interviewType;
	}

	public void setInterviewType(String interviewType) {
		this.interviewType = interviewType;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "interview_date", length = 10)
	public Date getInterviewDate() {
		return this.interviewDate;
	}

	public void setInterviewDate(Date interviewDate) {
		this.interviewDate = interviewDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "interview_time", length = 19)
	public Date getInterviewTime() {
		return this.interviewTime;
	}

	public void setInterviewTime(Date interviewTime) {
		this.interviewTime = interviewTime;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "interview")
	public List<InterviewApplicant> getInterviewApplicants() {
		return this.interviewApplicants;
	}

	public void setInterviewApplicants(List<InterviewApplicant> interviewApplicants) {
		this.interviewApplicants = interviewApplicants;
	}

}
