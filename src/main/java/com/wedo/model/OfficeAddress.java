package com.wedo.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "office", catalog = "infix_wedo")
public class OfficeAddress extends BaseModel implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7744373434708448357L;
	
	private String officeAddressId;
	private String addressStreet1;
	private String addressStreet2;
	private String city;
	private String pincode;
	private String state;
	private String country;
	private List<Client> clients = new ArrayList<Client>(0);

	public OfficeAddress() {
	}

	public OfficeAddress(String officeAddressId) {
		this.officeAddressId = officeAddressId;
	}

	public OfficeAddress(String officeAddressId, String addressStreet1, String addressStreet2, String city, String pincode,
			String state, String country, List<Client> clients) {
		this.officeAddressId = officeAddressId;
		this.addressStreet1 = addressStreet1;
		this.addressStreet2 = addressStreet2;
		this.city = city;
		this.pincode = pincode;
		this.state = state;
		this.country = country;
		this.clients = clients;
	}

	@Id
	@Column(name = "officeAddressId", unique = true, nullable = false, length = 21)
	public String getOfficeAddressId() {
		return this.officeAddressId;
	}

	public void setOfficeAddressId(String officeAddressId) {
		this.officeAddressId = officeAddressId;
	}

	@Column(name = "addressStreet1", length = 100)
	public String getAddressStreet1() {
		return this.addressStreet1;
	}

	public void setAddressStreet1(String addressStreet1) {
		this.addressStreet1 = addressStreet1;
	}

	@Column(name = "addressStreet2", length = 100)
	public String getAddressStreet2() {
		return this.addressStreet2;
	}

	public void setAddressStreet2(String addressStreet2) {
		this.addressStreet2 = addressStreet2;
	}

	@Column(name = "city", length = 25)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "pincode", length = 7)
	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	@Column(name = "state", length = 30)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "country", length = 30)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	public List<Client> getClients() {
		return this.clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}


}
