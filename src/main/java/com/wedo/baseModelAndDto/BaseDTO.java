package com.wedo.baseModelAndDto;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDTO {

	private Date createdOn;
	private Date updatedOn;
	private Integer isFlag=1;
}
