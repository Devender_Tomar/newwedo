package com.wedo.baseModelAndDto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseModelList<T> {

	private boolean status;
	private String message;
	private List<T> object;
	private Integer noOfPage[];

	public ResponseModelList() {
	}

	public ResponseModelList(boolean status, String message, List<T> object) {
		this.status = status;
		this.message = message;
		this.object = object;
	}

	public ResponseModelList(boolean status, String message, List<T> object, Integer[] noOfPage) {
		super();
		this.status = status;
		this.message = message;
		this.object = object;
		this.noOfPage = noOfPage;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<T> getObject() {
		return object;
	}

	public void setObject(List<T> object) {
		this.object = object;
	}

	public Integer[] getNoOfPage() {
		return noOfPage;
	}

	public void setNoOfPage(Integer[] noOfPage) {
		this.noOfPage = noOfPage;
	}

}
