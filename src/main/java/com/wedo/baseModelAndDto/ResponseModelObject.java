package com.wedo.baseModelAndDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseModelObject<T> {

	private boolean status;
	private String message;
	private T object;
	private Integer noOfPage[];

	public ResponseModelObject() {
	}

	public ResponseModelObject(boolean status, String message, T object) {
		this.status = status;
		this.message = message;
		this.object = object;
	}

	public ResponseModelObject(boolean status, String message, T object, Integer[] noOfPage) {
		this.status = status;
		this.message = message;
		this.object = object;
		this.noOfPage = noOfPage;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public Integer[] getNoOfPage() {
		return noOfPage;
	}

	public void setNoOfPage(Integer[] noOfPage) {
		this.noOfPage = noOfPage;
	}

}
