package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DepartmentDTO;
import com.wedo.dto.DesignationDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.service.DesignationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("designation")
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
public class DesignationController {
	@Autowired
	DesignationService service;

	ResponseModelList<DesignationDTO> response = new ResponseModelList<>();

	@RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<DesignationDTO> save(@RequestBody DesignationDTO designation) {
		// TODO Auto-generated method stub

		try {
			response = service.save(designation);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("{designationId}")
	public ResponseModelList<DesignationDTO> findById(@PathVariable String designationId) {
		// TODO Auto-generated method stub
		try {
			response = service.findById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("findAll")
	public ResponseModelList<DesignationDTO> findAll() {
		// TODO Auto-generated method stub
		try {
			response = service.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("findAllActive")
	public ResponseModelList<DesignationDTO> findAllActive() {
		// TODO Auto-generated method stub
		try {
			response = service.findAllActive();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<DesignationDTO> deleteAll() {
		// TODO Auto-generated method stub
		try {
			response = service.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@DeleteMapping("delete/{designationId}")
	public ResponseModelList<DesignationDTO> deleteById(@PathVariable String designationId) {
		// TODO Auto-generated method stub
		try {
			response = service.deleteById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

}
