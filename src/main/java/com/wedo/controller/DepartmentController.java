package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DepartmentDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("department")
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;

	ResponseModelList<DepartmentDTO> response = new ResponseModelList<DepartmentDTO>();

	@RequestMapping(value = "save", method = RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<DepartmentDTO> save(@RequestBody DepartmentDTO departmentDTO) {
		try {
			response = departmentService.save(departmentDTO);
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}
	
	@GetMapping("{departmentID}")
	public ResponseModelList<DepartmentDTO> findById(@PathVariable String  departmentID) {
		try {
			response = departmentService.findById(departmentID);
			log.info("Successfully findById department");
		} catch (Exception e) {
			log.error("Error in findById method");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("findAll")
	public ResponseModelList<DepartmentDTO> findAll() {
		try {
			response = departmentService.findAll();
			log.info("Successfully find All department");
		} catch (Exception e) {
			log.error("Error in findAll method");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("findAllActive")
	public ResponseModelList<DepartmentDTO> findAllActive() {
		try {
			response = departmentService.findAllActive();
			log.info("Successfully findAllActive department");
		} catch (Exception e) {
			log.error("Error in findAllActive method");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<DepartmentDTO> deleteAll() {
		try {
			response = departmentService.findAllActive();
			log.info("Successfully delete all department");
		} catch (Exception e) {
			log.error("Error in deleteAll method");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("delete/{departmentId}")
	public ResponseModelList<DepartmentDTO> deleteById(@PathVariable String departmentId) {
		try {
			response = departmentService.deleteById(departmentId);
			log.info("Successfully deleted department by Id");
		} catch (Exception e) {
			log.error("Error in deleteById method");
			e.printStackTrace();
		}
		return response;
	}

}
