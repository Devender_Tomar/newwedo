package com.wedo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.ClientDTO;
import com.wedo.dto.RoleDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.service.RoleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("role")
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
public class RoleController {

	@Autowired
	RoleService service;

	ResponseModelList<RoleDTO> response = new ResponseModelList<>();

	@RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<RoleDTO> save(@RequestBody Map roleDTO) {
		try {
			Gson  gson= new Gson();
			String strGson=gson.toJson(roleDTO);
			RoleDTO roleDto2=gson.fromJson(strGson, RoleDTO.class);
			response = service.save(roleDto2);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("{roleId}")
	public ResponseModelList<RoleDTO> findById(@PathVariable String roleId) {
		try {
			response = service.findById(roleId);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@DeleteMapping("{roleId}")
	public ResponseModelList<RoleDTO> deleteById(@PathVariable String roleId) {
		try {
			response = service.deleteById(roleId);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("all")
	public ResponseModelList<RoleDTO> findAll() {
		try {
			response = service.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@GetMapping("allActive")
	public ResponseModelList<RoleDTO> findAllActive() {
		try {
			response = service.findAllActive();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<RoleDTO> deleteAll() {
		try {
			response = service.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseModelList<>(false, "Internal Server Error", null);
		}
		return response;
	}

}
