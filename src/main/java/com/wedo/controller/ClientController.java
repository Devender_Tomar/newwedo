package com.wedo.controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.ClientDAO;
import com.wedo.dto.ClientDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.model.Client;
import com.wedo.model.ImageModel;
import com.wedo.service.ClientService;

import freemarker.template.utility.StringUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
@RestController
@RequestMapping("client")
public class ClientController {
	 
	ModelMapper mapper=new ModelMapper();

	@Autowired
	ClientService clientService;
	@Autowired
	ClientDAO clientDAO;
	
	private String path = "src\\main\\resources\\Profile\\";
	
	ResponseModelList<ClientDTO> modelList = new ResponseModelList<>();

	@RequestMapping(method = RequestMethod.POST,path = "save",  consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_MIXED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
	public ResponseModelList<ClientDTO> save(@RequestBody Map client) {
		try {
			Gson  gson= new Gson();
			String strGson=gson.toJson(client);
			ClientDTO clientDTO=gson.fromJson(strGson, ClientDTO.class);
			modelList = clientService.save(clientDTO);
			System.out.println(modelList);
			log.info("SuccessFully Saved Client");
		} catch (Exception e) {
			log.error("Client was not saved");
			e.printStackTrace();
		}
		return modelList;
	}

	@PostMapping("/uploadImage/{userId}")  
    public ResponseModelList<ClientDTO> handleFileUpload(@PathVariable String userId , @RequestParam("file") MultipartFile file) {
		
		try {
			Client client=clientDAO.findById(userId).get();
			client.setClientImage(this.write(file));
			client=clientDAO.save(client);
			modelList= new ResponseModelList<ClientDTO>(true, "Successfully Uplpoad", null);
		} catch (Exception e) {
			e.printStackTrace();
			modelList= new ResponseModelList<ClientDTO>(false, "Successfully Uplpoad", null);
		}
		
        return modelList;
    }  
	
	
	public ResponseModelList<ClientDTO> findById(String clientId) {
		try {
			modelList = clientService.findById(clientId);
			log.info("Client SuccessFully fetched");
		} catch (Exception e) {
			log.error("Client was not fetched");
			e.printStackTrace();
		}
		return modelList;
	}

	public ResponseModelList<ClientDTO> deleteById(String clientId) {
		try {
			modelList = clientService.deleteById(clientId);
			log.info("Client SuccessFully deleted");
		} catch (Exception e) {
			log.error("Client was not deleted");
			e.printStackTrace();
		}
		return modelList;
	}

	
	public ResponseModelList<ClientDTO> findAll() {
		try {
			modelList = clientService.findAll();
			log.info("Client Successfully Fetched");
		} catch (Exception e) {
			log.error("Error while fetched Client");
			e.printStackTrace();
		}
		return modelList;
	}

	public ResponseModelList<ClientDTO> deleteAll() {
		try {
			modelList = clientService.deleteAll();
			log.info("Client SuccessFully deleted");
		} catch (Exception e) {
			log.error("Error while deteting Client");
			e.printStackTrace();
		}
		return modelList;
	}
	
	@GetMapping("findAllActive")
	public ResponseModelList<ClientDTO> findAllActive() {
		try {
			modelList = clientService.findAllActive();
			log.info("Client SuccessFully Fetched");
		} catch (Exception e) {
			log.error("Error while fetching Client");
			e.printStackTrace();
		}
		return modelList;
	}
	
	public String write(MultipartFile file) {
		File convFile = new File(path + file.getOriginalFilename());
		String location="";
		try {
			convFile.createNewFile();
			location=path + file.getOriginalFilename();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(convFile);
		return location;
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET) 
	   public ResponseEntity<Object> downloadFile() throws IOException  {
	      String filename = "src\\main\\resources\\Profile\\Hydrangeas.jpg";
	      File file = new File(filename);
	      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	      HttpHeaders headers = new HttpHeaders();
	      
	      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
	      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
	      headers.add("Pragma", "no-cache");
	      headers.add("Expires", "0");
	      
	      ResponseEntity<Object> 
	      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
	         file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
	      
	      return responseEntity;
	   }

	 @RequestMapping(value = "/image", method = RequestMethod.POST,
	            produces = MediaType.IMAGE_JPEG_VALUE)
	    public ResponseEntity<InputStreamResource> getImage(@RequestBody String filename) throws IOException {

		 filename=filename.substring(0, filename.length());
		 String str=(new StringBuilder()).append("src\\main\\resources\\Profile\\").append(filename).toString();
		 System.err.println(str);
	      File file = new File(str);
	      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	        return ResponseEntity
	                .ok()
	                .contentType(MediaType.IMAGE_JPEG)
	                .body(new InputStreamResource(resource.getInputStream()));
	    }
	 
	 @RequestMapping(value = "/profile", method = RequestMethod.POST)
	    public ImageModel getProfileImage(@RequestBody String filename) throws IOException {

		 filename=filename.substring(0, filename.length());
		 String str=(new StringBuilder()).append("src\\main\\resources\\Profile\\").append(filename).toString();
		 System.err.println(str);
	      File file = new File(str);
	      byte[] picByte=readFileToByteArray(file);
	      ImageModel img = new ImageModel(FilenameUtils.getName(str), FilenameUtils.getExtension(str),picByte);
	     return img;
	     
	    }
	 private byte[] readFileToByteArray(File file){
		    FileInputStream fis = null;
		    byte[] bArray = new byte[(int) file.length()];
		    try{
		      fis = new FileInputStream(file);
		      fis.read(bArray);
		      fis.close();                   
		    }catch(IOException ioExp){
		      ioExp.printStackTrace();
		    }
		    return bArray;
		  }
	}
