package com.wedo.controller;

import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.EmployeeDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("employee")
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	ModelMapper mapper = new ModelMapper();
	
	ResponseModelList<EmployeeDTO> response = new ResponseModelList<>();

	@PostMapping(value = "save")
	public ResponseModelList<EmployeeDTO> save(@RequestBody Map employeeDTO) {
		try {
			Gson gson = new Gson();
			String convert = gson.toJson(employeeDTO);
			EmployeeDTO dto = mapper.map(convert, EmployeeDTO.class);
			response = employeeService.save(dto);
			log.info("Employee data Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method of Employee Controller");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("findById/{employeeId}")
	public ResponseModelList<EmployeeDTO> findById(@PathVariable String employeeId) {
		try {
			System.out.println("Employee Id   :  "+employeeId );
			response = employeeService.findById(employeeId);
		} catch (Exception e) {
			log.error("Error in findById method of Employee Controller");
			e.printStackTrace();
		}
		return response;
	}

	
	@GetMapping("findAll")
	public ResponseModelList<EmployeeDTO> findAll() {
		try {
			response = employeeService.findAll();
		} catch (Exception e) {
			log.error("Error in findAll method of Employee Controller");
			e.printStackTrace();
		}
		return response;	}

	@GetMapping("findAllActive")
	public ResponseModelList<EmployeeDTO> findAllActive() {
		try {
			response = employeeService.findAllActive();
		} catch (Exception e) {
			log.error("Error in findAllActive method of Employee Controller");
			e.printStackTrace();
		}
		return response;	}

	@GetMapping("findAllDeleted")
	public ResponseModelList<EmployeeDTO> findAllDeleted() {
		try {
			response = employeeService.findAllDeleted();
			log.info("Employee data Successfully saved");
		} catch (Exception e) {
			log.error("Error in findAllDeleted method of Employee Controller");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("deleteById/{employeeId}")
	public ResponseModelList<EmployeeDTO> deleteById(@PathVariable String employeeId) {
		try {
			response = employeeService.deleteById(employeeId);
		} catch (Exception e) {
			log.error("Error in deleteById method of Employee Controller");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<EmployeeDTO> deleteAll() {
		try {
			response = employeeService.deleteAll();
		} catch (Exception e) {
			log.error("Error in deleteAll method of Employee Controller");
			e.printStackTrace();
		}
		return response;
	}

}
