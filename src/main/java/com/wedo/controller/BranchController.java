package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.BranchDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.service.BranchService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("branch")
@CrossOrigin(origins = ConstantExtension.CROSS_ORIGIN)
public class BranchController{

	@Autowired
	BranchService branchService;

	ResponseModelList<BranchDTO> response = new ResponseModelList<BranchDTO>();

	@RequestMapping(value="save", method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<BranchDTO> save(@RequestBody BranchDTO branchDTO) {
		try {
			response = branchService.save(branchDTO);
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("findActive")
	public ResponseModelList<BranchDTO> findAllActive() {
		try {
			response = branchService.findAllActive();
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("findById/{branchId}")
	public ResponseModelList<BranchDTO> findById(@PathVariable String branchId) {
		try {
			response = branchService.findById(branchId);
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

	@GetMapping("branchs")
	public ResponseModelList<BranchDTO> findAll() {
		try {
			response = branchService.findAll();
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("delete")
	public ResponseModelList<BranchDTO> deleteAll() {
		try {
			response = branchService.deleteAll();
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

	@DeleteMapping("delete/{branchId}")
	public ResponseModelList<BranchDTO> deleteById(@PathVariable String branchId) {
		try {
			response = branchService.deleteById(branchId);
			log.info("Successfully saved");
		} catch (Exception e) {
			log.error("Error in save method");
			e.printStackTrace();
		}
		return response;
	}

}
