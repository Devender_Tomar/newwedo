package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.OfficeAddress;

@Repository
public interface OfficeAddressDAO extends JpaRepository<OfficeAddress, String>{

}
