package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Role;

@Repository
public interface RoleDAO extends JpaRepository<Role, String> {

	List<Role> findByIsFlag(Integer flag);
	List<Role> findByRole(String role);
	
}
