package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Address;

@Repository
public interface AddressDAO extends JpaRepository<Address, String>{
 
}
