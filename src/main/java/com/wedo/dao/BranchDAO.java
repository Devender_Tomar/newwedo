package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Branch;

@Repository
public interface BranchDAO extends JpaRepository<Branch, String>{

	List<Branch> findByIsFlag(Integer flag) ;
}
