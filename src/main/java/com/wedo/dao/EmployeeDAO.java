package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Employee;

@Repository
public interface EmployeeDAO extends JpaRepository<Employee, String>{
	
	List<Employee> findByIsFlag(Integer flag);
	
	
}
