package com.wedo.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Client;

@Repository
public interface ClientDAO extends JpaRepository<Client, String>{

	List<Client> findByIsFlag(int i);
}
