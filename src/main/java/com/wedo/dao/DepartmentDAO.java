package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Department;

@Repository
public interface DepartmentDAO extends JpaRepository<Department, String>{

	List<Department> findByIsFlag(Integer flag);
	
	
}
