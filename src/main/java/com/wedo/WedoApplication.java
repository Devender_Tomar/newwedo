package com.wedo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.wedo.helpers.HelperExtension;

@SpringBootApplication
public class WedoApplication {

	HelperExtension helperExtension= new HelperExtension();
	public static void main(String[] args) {
		SpringApplication.run(WedoApplication.class, args);

	}
	
	
	
	  @Configuration 
	  public class AdditionalResourceWebConfiguration implements WebMvcConfigurer {
	  
	  @Override public void addResourceHandlers(final ResourceHandlerRegistry registry) 
	  { 
		  registry.addResourceHandler("/Profile/**").addResourceLocations("file:Profile/"); } }
	 
	/*
	 * @Bean CommandLineRunner init (RoleDAO roleDao){ return args -> {
	 *
	 * if(roleDao.findAll().size()==0) { List<String> names = Arrays.asList("Admin",
	 * "Hr","Employee","client"); for(String name:names) { Role role=new Role();
	 * role.setRoleId("role"+helperExtension.getUniqueId()); role.setRoleType(name);
	 * role.setCreatedOn(helperExtension.getDateTime());
	 * role.setUpdatedOn(helperExtension.getDateTime()); roleDao.save(role); } } };
	 * }
	 */

}
